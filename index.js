const request = require('request')

const options = {
  host: 'https://api.ote-godaddy.com',
  client_id: null,
  client_secret: null
}

const GET = 'get'
const POST = 'post'

const configure = (o) => {
  if (!o)
    return godaddy

  Object.keys(o).forEach(name => {
    options[name] = o[name]
  })

  return godaddy
}

const godaddy = {
  domains: {},
  shoppers: {},
  orders: {},
  sections: ['domains', 'shoppers', 'orders'],
  configure,
  FULL: 'FULL',
  FAST: 'FAST'
}


const makeFunction = function(definition) {
  Object.keys(definition.params).forEach(name => {
    const def = definition.params[name]
    if (!def) {
      console.log('Definition for parameter ' + name + ' not found')
      process.exit()
    }
  })

  const call = (args, cb) => {
    const headers = {}
    const body = {}
    const qs = {}

    if (!headers['Authorization'] && options.client_id && options.client_secret)
      headers.Authorization = `sso-key ${options.client_id}:${options.client_secret}`

    const parseArg = arg_name => {
      const def = definition.params[arg_name]
      const arg_val = args[arg_name]

      if (!def)
        return cb('Unknown argument ' + arg_name)

      if (def.type === 'header')
        headers[def.name] = arg_val

      if (def.type === 'body')
        body[def.name] = arg_val

      if (def.type === 'qs')
        qs[def.name] = arg_val
    }

    Object.keys(args).forEach(parseArg)

    const callback = (err, res, body) => {
      if (err)
        return cb(err)

      if (res.statusCode > 299)
        return cb(body)

      if (definition.processor)
        definition.processor(body, cb)
      else
        cb(null, body)
    }

    const method = request[definition.method].bind(request)

    const arguments = {
      headers,
      json: true
    }

    arguments.uri = options.host + definition.uri
    arguments.qs = qs
    arguments.body = body

    if (definition.preprocess)
      definition.preprocess.call(null, arguments, args)

    method(arguments, callback)
  }

  call.definition = definition

  return call
}

const params = {
  qs: {},
  body: {},
  header: {}
}

const addParam = (type, name, attrs) => {
  const def = {
    name,
    type
  }

  params[type][name] = def

  if (!attrs)
    return

  Object.keys(attrs).forEach(i => {
    def[i] = attrs[i]
  })
}

addParam('qs', 'domain')

addParam('qs', 'check_type', {
  name: 'checkType'
})

addParam('qs', 'for_transfer', {
  name: 'forTransfer',
  default_value:false
})

addParam('qs', 'query')
addParam('qs', 'tlds')
addParam('qs', 'privacy')
addParam('qs', 'limit')

addParam('header', 'shopper_id', {
  name: 'X-Shopper-Id'
})

addParam('header', 'market_id', {
  name: 'X-Market-Id'
})


addParam('body', 'domain')
addParam('body', 'domains')
addParam('body', 'consent')
addParam('body', 'period')
addParam('body', 'name_servers', {
  name: 'nameServers'
})
addParam('body', 'auto_renew', {
  name: 'renewAuto'
})
addParam('body', 'privacy')
addParam('body', 'contact_registrant', {
  name: 'contactRegistrant'
})
addParam('body', 'contact_admin', {
  name: 'contactAdmin'
})
addParam('body', 'contact_tech', {
  name: 'contactTech'
})
addParam('body', 'contact_billing', {
  name: 'contactBilling'
})
addParam('body', 'market_id', {
  name: 'marketId'
})

addParam('body', 'email')
addParam('body', 'first_name', {
  name: 'nameFirst'
})

addParam('body', 'last_name', {
  name: 'nameLast'
})

addParam('body', 'password')
addParam('body', 'external_id', {
  name: 'externalId'
})

godaddy.domains.suggest = makeFunction({
  method: GET,
  uri: '/v1/domains/suggest',
  processor: (body, cb) => {
    cb(null, body.map( r => r.domain ))
  },
  params: {
    shopper_id: params.header.shopper_id,
    query: params.qs.query,
    limit: params.qs.limit,
    tlds: params.qs.tlds
  }
})

godaddy.domains.available = makeFunction({
  method: GET,
  uri: '/v1/domains/available',
  params: {
    domain: params.qs.domain,
    check_type: params.qs.check_type,
    for_transfer: params.qs.for_transfer
  }
})

godaddy.domains.bulkAvailable = makeFunction({
  method: POST,
  uri: '/v1/domains/available',
  params: {
    check_type: params.qs.check_type,
    domains: params.body.domains
  },
  preprocess: (request, args) => {
    request.body = args.domains
  }
})

godaddy.domains.purchase = makeFunction({
  method: POST,
  uri: '/v1/domains/purchase',
  params: {
    shopper_id: params.header.shopper_id,
    domain: params.body.domain,
    consent: params.body.consent,
    period: params.body.period,
    name_servers: params.body.name_servers,
    auto_renew: params.body.auto_renew,
    privacy: params.body.privacy,
    contact_registrant: params.body.contact_registrant,
    contact_admin: params.body.contact_admin,
    contact_tech: params.body.contact_tech,
    contact_billing: params.body.contact_billing,
  }
})

godaddy.domains.getAgreements = makeFunction({
  method: GET,
  uri: '/v1/domains/agreements',
  params: {
    market_id: params.header.market_id,
    tlds: params.qs.tlds,
    privacy: params.qs.privacy,
    for_transfer: params.qs.for_transfer
  }
})

godaddy.shoppers.createSubAccount = makeFunction({
  method: POST,
  uri: '/v1/shoppers/subaccount',
  params: {
    email: params.body.email,
    password: params.body.password,
    first_name: params.body.first_name,
    last_name: params.body.last_name,
    external_id: params.body.external_id,
    market_id: params.body.market_id,
  }
})

module.exports = configure