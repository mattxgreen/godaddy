#!/usr/bin/env node

const godaddy = require('./index.js')()
const vorpal = require('vorpal')()
const prettyjson = require('prettyjson')

const log = (data) => {
  console.log(prettyjson.render(data))
}

vorpal.delimiter('Godaddy >')

vorpal
  .command('set keys <id> <secret>')
  .action(setKeys)

function setKeys(args, callback) {
  godaddy.configure({
    client_id: args.id,
    client_secret: args.secret
  })

  vorpal.delimiter(`${args.id.substr(0,5)}@Godaddy>`)
  callback()
}

godaddy.sections.forEach(section_name => {
  const section = godaddy[section_name]

  Object.keys(section).forEach(fn_name => {
    const fn = section[fn_name]
    const def = fn.definition

    const call = function(args, callback) {

      fn.call(null, args.options, (err, res) => {
        if (err)
          log(err)
        else
          log(res)

        callback(err, res)
      })
    }

    const command = vorpal
    .command(`${section_name} ${fn_name}`)

    Object.keys(def.params).forEach(n => {
      command.option(`--${n} <${n}>`)
    })

    command.action(call)
  })
})

vorpal.show()
